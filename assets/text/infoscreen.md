# About Solar Hunt

![line break](resource:assets/images/line_break.png)

## :dart: What is the goal of this?

![line break](resource:assets/images/line_break.png)

The UK has about 1 million rooftop solar installations. As well as feeding power directly into homes, 
these panels supply power back into the national grid when buildings can't use all the power they're producing.
This is a good thing, because it means we can share clean energy around and reduce carbon emissions, 
to help tackle climate change.

The thing is: no single person knows exactly where all these solar panels are. 
This means that it's difficult for the National Grid to predict when and where there will be surges or drops in solar power output.
In order to keep the power system stable, power stations currently have to be kept running in the background - burning fossil fuels.
If, instead, we had a record of where exactly these panels were, 
we could use short term weather forecasts to anticipate solar power output much better, and turn down those power stations.

![line break](resource:assets/images/line_break.png)

**Put simply - if we know where the nation's rooftop solar panels are, we can make better use of them - 
saving around 100,000 tonnes of carbon each year.**

![line break](resource:assets/images/line_break.png)

### This is where you come in!

[Open Climate Fix](https://openclimatefix.org/) and [Possible](https://www.wearepossible.org/) have teamed up to create the Solar Hunt app,
so that with the help of individuals and communities across the country, we can produce the most detailed dataset of solar panels in the UK.
The dataset we create will be "open" - completely free for the public to use.

![line break](resource:assets/images/line_break.png)

## :+1: I'm in - how can I help?

![line break](resource:assets/images/line_break.png)

Every time you see a solar panel when you are out and about, take a picture of it, check it's position is correct, and hit upload. It's that simple!
This is what to look out for:

![example solar panel image](resource:assets/images/example_solar.jpg)

Things to consider:
- Solar panels are generally located on the roofs of houses, so remember to look up!
- Taking pictures of solar panels on somebody's house from the road or pavement is perfectly legal,
but please be considerate of people's privacy when doing this.
- If you do catch someone in your photo, don't worry. All uploaded pictures will automatically blur out faces if they are in the picture.

![line break](resource:assets/images/line_break.png)

## :earth_africa: I want the bigger picture!


![line break](resource:assets/images/line_break.png)

This is part of a wider project to takle the UK's hefty carbon footprint. 
Open Climate Fix are dedicated to using computer science and machine learning to help tackle climate change.
Because finding all these panels means looking for a million rooftops, they plan to teach a computer to recognise solar installations from aeriel photographs.
But in order to teach a computer, you need to provide it with plenty of examples to learn from.

That means manually locating and imaging solar panels on rooftops in the world around us - sol the computer can learn what they look like.
By collaberating with the [OpenStreetMap](https://www.openstreetmap.org/#map=6/54.910/-3.432) community, already 10% of UK panels have been mapped!
But to create a really powerful computer solar power hunter, we need to try to double that figure. 
At that point, the machines can take over finding the remaining 80% without anyone lifting a finger.

![line break](resource:assets/images/line_break.png)

**And it doesn't stop there! Once we've got this working it can be used to map panels all around the world - 
providing data to help clean energy push out dirty fossil fuels.**

![line break](resource:assets/images/line_break.png)

## :camera: What is happening to the pictures?

![line break](resource:assets/images/line_break.png)

The pictures are uploaded to [Mapillary](https://www.mapillary.com/), an open database of street-view pictures.
the pictures will then be processed and the size and orientation of the panels will be captured. 
This data will then be entered into OpenStreetMap, an open database for geographical data. 
If you would like to access the data you simply need to go to OpenStreetMap and run a search for solar panels.

Mapillary stores the pictures, and uses them for automatically detecting map features.
Your pictures will be publically available on the Mapillary platform. However, the photos won't be on OpenStreetMap -
that's just the map data, so that will just have the solar panel's locations and some other details.