## Sign in with Mapillary?

#### Mappilary is the service that processes and uploads submitted images to OpenStreetMap. Linking an account enables you to view your submissions on Mapillary\'s website.

![line break](resource:assets/images/line_break.png)
