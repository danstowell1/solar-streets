# solar_hunt

Flutter frontend for the OpenClimateFix solar streets project.

## Installing Flutter
For help getting started with Flutter, view the
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference. 

- Set up with VSCode: https://flutter.dev/docs/get-started/editor?tab=vscode
- Run the app: https://flutter.dev/docs/get-started/test-drive?tab=vscode
